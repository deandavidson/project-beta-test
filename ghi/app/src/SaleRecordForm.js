import React, { useState, useEffect } from 'react';

function SaleForm ({ getSale }) {
    const [salesPerson, setsalesPerson] = useState([]);
    const [customer, setCustomer]= useState([]);
    const [automobiles, setAutomobiles] = useState([]);

    const [salesPersons, setsalesPersons] = useState('');
    const [customers, setCustomers] = useState('');
    const [auto, setAuto] = useState('');
    const [price, setPrice] =useState('');

      const handlesalesPersonsChange = (event) => {
        const value = event.target.value;
        setsalesPersons(value)
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomers(value)
    }

    const handleAutoChange = (event) => {
      const value = event.target.value;
      setAuto(value)
  }

    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value)
    }

    const handleSubmit = async (event) =>{
        event.preventDefault();
        const data = {};
        data.automobile = auto
        data.salesperson = salesPersons
        data.customer = customers
        data.price = price

        const saleUrl = "http://localhost:8090/api/sale/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };

        const response = await fetch (saleUrl, fetchConfig);
        if (response.ok){
            const newSaleRecord = await response.json();
            getSale();

            console.log(newSaleRecord)

            setAuto('');
            setsalesPersons('');
            setCustomers('');
            setPrice('');
        }

    }


    const fetchSalespersonData = async()=> {
        const url = "http://localhost:8090/api/salesperson/"
        const response = await fetch (url)
        if (response.ok){
            const data = await response.json()
            setsalesPerson(data.salesperson)
        }
    }

    const fetchCustomerData = async()=> {
        const url = "http://localhost:8090/api/customer/"
        const response = await fetch (url)
        if (response.ok){
            const data = await response.json()
            setCustomer(data.customer)
        }
    }

    const fetchAutoData = async () => {
      const url = "http://localhost:8100/api/automobiles/"
      const response = await fetch (url)
      if (response.ok){
          const data = await response.json();
          setAutomobiles(data.autos)

      }
  }


    useEffect (() => {
        fetchAutoData();
        fetchSalespersonData();
        fetchCustomerData();
    }, [])

    return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">

              <h1>Add a sale!</h1>

              <form onSubmit ={handleSubmit} id="create-new-sale-record-form">
              <div className="mb-3">
                <label htmlFor="name">Automobile VIN</label>
                  <select onChange={handleAutoChange} required name="automobile" id="automobile" className="form-select" >
                    <option value="">Select an automobile</option>
                    {automobiles.map(auto =>{
                      return (
                          <option key={auto.vin} value={auto.href}>
                              {auto.vin}
                          </option>
                      )
                    })}
                  </select>
                </div>

                <div className="mb-3">
                  <label htmlFor="name">Salesperson</label>
                    <select onChange={handlesalesPersonsChange} required name="salesperson" id="salesperson" className="form-select" >
                      <option value="">Select a sales person</option>
                      {salesPerson.map(person => {
                        return (
                            <option key={person.id} value={person.id}>
                                {person.name}
                            </option>
                        )
                      })}
                    </select>
                </div>

                <div className="mb-3">
                  <label htmlFor="name">Customer</label>
                    <select onChange={handleCustomerChange} required name="customer" id="customer" className="form-select" >
                      <option value="">Select a customer</option>
                      {customer.map(customer =>{
                        return (
                            <option key={customer.id} value={customer.id}>
                                {customer.name}
                            </option>
                        )
                      })}
                    </select>
                </div>

                <div className="form-floating mb-3">
                  <input onChange={handlePriceChange}placeholder="price" required type="number" name="price" id="price" className="form-control" />
                    <label htmlFor="price">Enter sale price </label>
                </div>

                <button className="btn btn-primary" title="Great job!">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
)

}


export default SaleForm;
