import React, { useEffect, useState } from 'react';
import './index.css';

function NewTech() {
	const [tech_name, setTech_name] = useState('');
	const [tech_number, setTech_number] = useState('');

	const handletech_name = (event) => {
		const value = event.target.value;
		setTech_name(value);
	};
	const handletech_number = (event) => {
		const value = event.target.value;
		setTech_number(value);
	};

	const handleSubmit = async (event) => {
		event.preventDefault();

		const data = {};

		data.tech_name = tech_name;
		data.tech_number = tech_number;

		const locationUrl = 'http://localhost:8080/api/techs/';
		const fetchConfig = {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json',
			},
		};

		try {
			const response = await fetch(locationUrl, fetchConfig);
			setTech_name('');
			setTech_number('');
		} catch (error) {
			console.log(error);
		}
	};

	// useEffect(() => {
	// 	fetchData();
	// }, []);

	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h1>Create a new Tech</h1>
					<form onSubmit={handleSubmit} id="create-tech-form">
						<div className="form-floating mb-3">
							<input
								value={tech_name}
								onChange={handletech_name}
								placeholder="Tech Name"
								required
								type="text"
								name="tech_name"
								id="tech_name"
								className="form-control"
							/>
							<label htmlFor="tech_name">Tech Name</label>
						</div>
						<div className="form-floating mb-3">
							<input
								value={tech_number}
								onChange={handletech_number}
								placeholder="Tech Number"
								required
								type="text"
								name="tech_number"
								id="tech_number"
								className="form-control"
							/>
							<label htmlFor="tech_number">Tech Number</label>
						</div>
						<button className="btn btn-primary">Create</button>
					</form>
				</div>
			</div>
		</div>
	);
}

export default NewTech;
