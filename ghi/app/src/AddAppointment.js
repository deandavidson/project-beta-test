import React, { useEffect, useState } from 'react';
import './index.css';

function NewAppointment() {
	const [vin, setVin] = useState('');
	const [customer_name, setCustomer_name] = useState('');
	const [appointment_time, setappointment_time] = useState('');
	const [reason, setReason] = useState('');
	const [service_tech, setService_tech] = useState('');
	const [techs, setTechs] = useState([]);

	const handleVin = (event) => {
		const value = event.target.value;
		setVin(value);
	};
	const handlecustomer_name = (event) => {
		const value = event.target.value;
		setCustomer_name(value);
	};
	const handleappointment_time = (event) => {
		const value = event.target.value;
		setappointment_time(value);
	};
	const handleReason = (event) => {
		const value = event.target.value;
		setReason(value);
	};
	const handleService_tech = (event) => {
		const value = event.target.value;
		setService_tech(value);
	};

	const handleSubmit = async (event) => {
		event.preventDefault();

		const data = {};

		data.vin = vin;
		data.customer_name = customer_name;
		data.appointment_time = appointment_time;
		data.reason = reason;
		data.service_tech = service_tech;

		const locationUrl = 'http://localhost:8080/api/services/';
		const fetchConfig = {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json',
			},
		};

		try {
			const response = await fetch(locationUrl, fetchConfig);
			setVin('');
			setCustomer_name('');
			setappointment_time('');
			setReason('');
			setService_tech('');
		} catch (error) {
			console.log(error);
		}
	};

	const fetchData = async () => {
		const url = 'http://localhost:8080/api/techs/';

		const response = await fetch(url);

		if (response.ok) {
			const data = await response.json();
			setTechs(data.techs);
		}
	};

	useEffect(() => {
		fetchData();
	}, []);

	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h1>Create a new Appointment</h1>
					<form onSubmit={handleSubmit} id="create-appointment-form">
						<div className="form-floating mb-3">
							<input
								value={vin}
								onChange={handleVin}
								placeholder="Vin Number"
								required
								type="text"
								name="vin"
								id="vin"
								className="form-control"
							/>
							<label htmlFor="vin">Vin Number</label>
						</div>
						<div className="form-floating mb-3">
							<input
								value={customer_name}
								onChange={handlecustomer_name}
								placeholder="Customer Name"
								required
								type="text"
								name="customer_name"
								id="tecustomer_name"
								className="form-control"
							/>
							<label htmlFor="customer_name">Customer Name</label>
						</div>
						<div className="form-floating mb-3">
							<input
								value={appointment_time}
								onChange={handleappointment_time}
								placeholder="Appointment Time"
								required
								type="datetime-local"
								name="appointment_time"
								id="appointment_time"
								className="form-control"
							/>
							<label htmlFor="appointment_time">
								Appointment Time
							</label>
						</div>
						<div className="form-floating mb-3">
							<input
								value={reason}
								onChange={handleReason}
								placeholder="Reason"
								required
								type="text"
								name="reason"
								id="reason"
								className="form-control"
							/>
							<label htmlFor="reason">Reason</label>
						</div>
						<div className="mb-3">
							<select
								value={service_tech}
								onChange={handleService_tech}
								required
								name="service_tech"
								id="service_tech"
								className="form-select"
							>
								<option value="">Choose a Tech</option>
								{techs.map((tech) => {
									return (
										<option key={tech.id} value={tech.id}>
											{tech.tech_name}
										</option>
									);
								})}
							</select>
						</div>

						<button className="btn btn-primary">Create</button>
					</form>
				</div>
			</div>
		</div>
	);
}

export default NewAppointment;
