import React, { useEffect, useState } from 'react';
import './index.css';
import DeleteAppt from './DeleteAppointment';
import FinishedAppt from './FinishedAppointment';

function AppointmentList(props) {
	const [autos, setAutos] = useState('');

	const fetchData = async () => {
		const url = 'http://localhost:8100/api/automobiles/';
		const response = await fetch(url);
		const data = await response.json();
		const arr1 = [];
		for (let auto of data.autos) {
			arr1.push(auto.vin);
		}
		setAutos(arr1);
	};

	useEffect(() => {
		fetchData();
	}, []);

	return (
		<table className="table table-striped">
			<thead>
				<tr>
					<th>Customer Name</th>
					<th>Vin Number</th>
					<th>Appointment Time</th>
					<th>Reason</th>
					<th>Assigned Technichian</th>
					<th>Is VIP?</th>
				</tr>
			</thead>
			<tbody>
				{props.services.map((service) => {
					const isVIPvin = autos.includes(service.vin);
					return (
						<tr key={service.id}>
							<td>{service.customer_name}</td>
							<td>{service.vin}</td>
							<td>{service.appointment_time}</td>
							<td>{service.reason}</td>
							<td>{service.service_tech}</td>
							<td>{isVIPvin && 'Yes'}</td>
							<td>
								<DeleteAppt id={service.id} />{' '}
								<FinishedAppt id={service.id} />{' '}
							</td>
						</tr>
					);
				})}
			</tbody>
		</table>
	);
}

export default AppointmentList;
