import React, { useEffect, useState } from 'react';
import './index.css';

function NewModel() {
	const [name, setName] = useState('');
	const [picture_url, setpicture_url] = useState('');
	const [manufacturer_id, setmanufacturer_id] = useState('');
	const [manufacturers, setmanufacturers] = useState([]);

	const handleName = (event) => {
		const value = event.target.value;
		setName(value);
	};
	const handlepicture_url = (event) => {
		const value = event.target.value;
		setpicture_url(value);
	};
	const handlemanufacturer_id = (event) => {
		const value = event.target.value;
		setmanufacturer_id(value);
	};

	const handleSubmit = async (event) => {
		event.preventDefault();

		const data = {};

		data.name = name;
		data.picture_url = picture_url;
		data.manufacturer_id = manufacturer_id;

		const locationUrl = 'http://localhost:8100/api/models/';
		const fetchConfig = {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json',
			},
		};

		try {
			const response = await fetch(locationUrl, fetchConfig);
			setName('');
			setpicture_url('');
			setmanufacturer_id('');
		} catch (error) {
			console.log(error);
		}
	};

	const fetchData = async () => {
		const url = 'http://localhost:8100/api/manufacturers/';

		const response = await fetch(url);

		if (response.ok) {
			const data = await response.json();
			setmanufacturers(data.manufacturers);
		}
	};

	useEffect(() => {
		fetchData();
	}, []);

	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h1>Create a new Model</h1>
					<form onSubmit={handleSubmit} id="create-appointment-form">
						<div className="form-floating mb-3">
							<input
								value={name}
								onChange={handleName}
								placeholder="Name"
								required
								type="text"
								name="name"
								id="name"
								className="form-control"
							/>
							<label htmlFor="name">Model Name</label>
						</div>
						<div className="form-floating mb-3">
							<input
								value={picture_url}
								onChange={handlepicture_url}
								placeholder="Model Picture"
								required
								type="text"
								name="picture_url"
								id="picture_url"
								className="form-control"
							/>
							<label htmlFor="picture_url">
								Picture URL of Model
							</label>
						</div>

						<div className="mb-3">
							<select
								value={manufacturer_id}
								onChange={handlemanufacturer_id}
								required
								name="manufacturer_id"
								id="manufacturer_id"
								className="form-select"
							>
								<option value="">Choose a Manufacturer</option>
								{manufacturers.map((manufacturer) => {
									return (
										<option
											key={manufacturer.id}
											value={manufacturer.id}
										>
											{manufacturer.name}
										</option>
									);
								})}
							</select>
						</div>

						<button className="btn btn-primary">Create</button>
					</form>
				</div>
			</div>
		</div>
	);
}

export default NewModel;
