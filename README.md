# CarCar

#### Team:
* Dean Davidson - Appointment/Service microservice
* Hana Machrus - Sales microservice


## How to Run this Application
1. Open up docker desktop in your terminal:
2. git clone project to your local machine
3. Run the following commands in your terminal to create the database
```
docker volume create beta-data
docker-compose build
docker-compose up
```
4. Run `docker-compose build `
5. Run `docker-compose up` check your docker desktop application to ensure that all containers have started and are actively running on your browser of choice, then navigate to http://localhost:3000/
This is the homepage of the application.

You're now ready to manage your dealership information!

##### <p style="text-align: center;"> ↓ Click on the dropdowns in each section below for extra details and steps ↓ </p>


## CRUD Routes, API Documentation

### Appointment Service API

<details><summary><b> About the API:</b></summary>
<p>The service/appointment microservice allows the car dealership to be able to track and manage their entire services department. This includes the technicians, as well as the service appointments themselves. In order to properly use the application. The user should first create a technician in the database with the "Create Tech" button in the Nav bar. This will load a form to submit a new tech. Type the techs name and the techs number. The number for the technician should be a positive small integer. You can then navigate to the technician list page, refresh the browser, and your technician should now be displayed on the web page. Next, you can create a service appointment for a customer. Navigate to the Create Service button in the Nav bar. This will take you to a form to submit a new service appointment. Fill out the form with the Vin number of the vehicle, customer name, appointment date and time, reason for appointment, and the desired technician from the drop down bar. Click create. Now navigate to the Services List button in the nav bar. Refresh the browser. This now displays the service appointment that you just created via that form. If the vin input into the appointment form, matches a vin in the automobile inventory database, a "Yes" will appear in the VIP column. Letting you know to give this customer the VIP treatment for purchasing their vehicle from the dealership. When a service has been completed, click the "Finished Appointment" button on the services list. Refresh the page and that service will no longer display on the web page. When a service has been cancelled, click the "Delete Appointment" button on the services list. Refresh the page and that service will no longer display on the web page. Similarly, when a technician is fired or quits, click the "Delete Tech" button on the Technician list. Refresh the page and that tech will no longer display on the web page. Lastly, navigate to the "search vin" button in the nav bar. This allows the user to enter the vin of any vehicle. If the vin currently exists in the database, it will display all appointments for that given vin. If their isn't a matching vin in the database, nothing will change/render on the screen.</p>
</details>

#### Appointment Requests:
| Action                            | Method | URL                                                  |
|-----------------------------------|:------:|------------------------------------------------------|
| View list of service appointments | GET    | `http://localhost:8080/api/services/`                |
| Add a new service appointment     | POST   | `http://localhost:8080/api/services/`                |
| View appointment details          | POST   | `http://localhost:8080/api/services/«appointmentID»` |
| Delete appointment                | DELETE | `http://localhost:8080/api/services/«appointmentID»` |




<details>
<summary> Sample GET request to `/api/services/`</summary>

###### Returns:

```
{
"services": [
    {
        "id": 7,
        "vin": "23452345234526",
        "customer_name": "Billy Bob",
        "appointment_time": "2023-01-26T08:21:00+00:00",
        "reason": "Stuff",
        "service_tech": "Dean Davidson"
    },
}
```
</details>

<details>
<summary> Sample POST request to `/api/services/` </summary>

###### Request body:
```
{
    "service_tech": 9,
    "vin": 23452345234526,
    "reason": "",
    "customer_name": "Sally May",
    "appointment_time": "2025-05-01"
}

```
###### Returns (status code 200):
```
{
    "id": 9,
    "vin": 23452345234526,
    "customer_name": "Sally May",
    "appointment_time": "2025-05-01",
    "reason": ""
}
```

</details>


#### Technician requests:

| Action                      | Method | URL                                              |
|-----------------------------|:------:|--------------------------------------------------|
| View list of technicians    | GET    | `http://localhost:8080/api/techs/`               |
| Add a new technician        | POST   | `http://localhost:8080/api/techs/`               |
| View technician details     | POST   | `http://localhost:8080/api/techs/«technicianID»` |
| Delete technician           | DELETE | `http://localhost:8080/api/techs/«technicianID»` |

<details>
<summary> Sample GET request to `/api/techs/` </summary>

###### Returns:

```
{
    "techs": [
        {
        "id": 1,
        "tech_name": "Fred Bear",
        "tech_number": 1
},
```

</details>

<details>
<summary> Sample POST request to `/api/techs/` </summary>

###### Request body:
```
{
    "tech_name": "Brandon Dix",
    "tech_number": 15
}
```

###### Returns:
```
{
    "id": 8,
    "tech_name": "Brandon Dix",
    "tech_number": 15
}
```

</details>

___


### Sales API

<details><summary><b> About the API:</b></summary>
<p>The sales API includes all the information necessary to add employees to the database, add potential customers to the database, and finally create a sales record of cars sold. The Salesperson and Customer models function independently, while the AutomobileVO (automobile value object) refers back to the Inventory API to grab information about each car's VIN in order to catalog it when a sale is made. </p>
</details>

#### Salespeople Actions:

| Action                              | Method | URL                                                    |
|-------------------------------------|:------:|--------------------------------------------------------|
| View list of salespeople            | GET    | `http://localhost:8090/api/salesperson/`               |
| View individual salesperson details | GET    | `http://localhost:8090/api/salesperson/«salespersonID»`|
| Add a new salesperson               | POST   | `http://localhost:8090/api/salesperson/`               |
| Delete salesperson's record         | DELETE | `http://localhost:8090/api/salesperson/«salespersonID»`|

<details>
<summary>Click here for a sample salesperson POST request</summary>

###### Example:
```
{
	"name": "John McSales",
	"employee_number": 90877513
}
```
</details>




#### Customer Actions:
| Action                           | Method | URL                                                  |
|----------------------------------|:------:|------------------------------------------------------|
| View list of potential customers | GET    | `http://localhost:8090/api/customer/`                |
| View individual customer details | GET    | `http://localhost:8090/api/customer/«customerID»`    |
| Add a new customer               | POST   | `http://localhost:8090/api/customer/`                |
| Delete a customer's record       | DELETE | `http://localhost:8090/api/customer/«customerID»`    |

<details>
<summary>Click here for a sample customer POST request</summary>

###### Example:
```
{
	"name": "Sample Customer",
	"address": "123 Main Street",
	"phone_number": "(123) 456-7890"
}
```
</details>




#### Sales Record Actions
| Action                | Method | URL                                       |
|-----------------------|:------:|-------------------------------------------|
| View list of sales    | GET    | `http://localhost:8090/api/sale/`         |
| View sale details     | GET    | `http://localhost:8090/api/sale/«saleID»` |
| Add a new sale record | POST   | `http://localhost:8090/api/sale/`         |
| Delete a sale record  | DELETE | `http://localhost:8090/api/sale/«saleID»` |



<details>
<summary>Click here for a sample sales record POST request</summary>

###### Example:

```
{
    "automobile": "/api/automobiles/YS3AL76L1R7002116/",
	"price": 19000,
	"salesperson": 1,
	"customer": 1
}
```
Where automobile references a vehicle in the inventory using `/api/automobiles/«VIN»`, salesperson is referenced by `«salespersonID»`, and customer is referenced by `«customerID»`
Upon creating a sales record, the vehicle selected will automatically be marked as sold!

</details>

___

### Inventory API

#### Automobile Actions:
| Action                   | Method | URL                                           |
|--------------------------|:------:|-----------------------------------------------|
| View list of automobiles | GET    | `http://localhost:8100/api/automobiles/`      |
| Create a new automobile  | POST   | `http://localhost:8100/api/automobiles/`      |
| View automobile details  | GET    | `http://localhost:8100/api/automobiles/«VIN»` |
| Delete an automobile     | DELETE | `http://localhost:8100/api/automobiles/«VIN»` |

<details>
<summary>Click here for a sample automobile POST & GET request</summary>

###### Request Body:
```
{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}
```

###### Returns:
```
{
    "href": "/api/automobiles/1C3CC5FB2AN120174/",
    "id": 1,
    "color": "yellow",
    "year": 2013,
    "vin": "1C3CC5FB2AN120174",
    "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "Sebring",
        "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
    "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Daimler-Chrysler"
    }
  }
}
```


</details>

#### Manufacturer Actions:
| Action                     | Method | URL                                            |
|----------------------------|:------:|------------------------------------------------|
| View list of manufacturers | GET    | `http://localhost:8100/api/manufacturers/`     |
| Create a new manufacturer  | POST   | `http://localhost:8100/api/manufacturers/`     |
| View manufacturer details  | GET    | `http://localhost:8100/api/manufacturers/«ID»` |
| Delete a manufacturer      | DELETE | `http://localhost:8100/api/manufacturers/«ID»` |

<details>
<summary>Click here for a sample manufacturer POST & GET request</summary>

###### Request Body:

```
{
  "name": "Chrysler"
}
```
This only requires the manufacturer name
###### Returns:
```
{
  "href": "/api/manufacturers/1/",
  "id": 1,
  "name": "Chrysler"
}
```

</details>

#### Models Actions:
| Action                      | Method | URL                                          |
|-----------------------------|:------:|----------------------------------------------|
| View list of vehicle models | GET    | `http://localhost:8100/api/models/`          |
| Create a new vehicle model  | POST   | `http://localhost:8100/api/models/`          |
| View vehicle model details  | GET    | `http://localhost:8100/api/models/«modelID»` |
| Delete a vehicle model      | DELETE | `http://localhost:8100/api/models/«modelID»` |

<details>
<summary>Click here for a sample models POST request</summary>

###### Request body:

```
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 1
}
```
###### Returns:
```
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg"
}
```
</details>

___

## Design

### Diagram of Models & Inventory Integration

![Model integration](https://i.imgur.com/p7qB6ir.png "Cool diagram of our APIs!")
