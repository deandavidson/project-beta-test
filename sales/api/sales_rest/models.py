from django.db import models
from django.urls import reverse
from django.core.exceptions import ObjectDoesNotExist

# Create your models here.

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, unique=True)
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin

class SalesPerson(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.IntegerField()

    def __str__(self):
        return self.name

class Customer(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=200)

    def __str__(self):
        return self.name

class SalesRecord(models.Model):
    price = models.PositiveIntegerField()
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name = "salesrecord",
        on_delete=models.PROTECT
    )

    salesperson = models.ForeignKey(
        SalesPerson,
        related_name = "salesrecord",
        on_delete=models.PROTECT
    )

    customer = models.ForeignKey(
        Customer,
        related_name = "salesrecord",
        on_delete=models.PROTECT
    )
