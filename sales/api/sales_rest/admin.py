from django.contrib import admin
from sales_rest.models import SalesRecord, AutomobileVO, Customer, SalesPerson



admin.site.register(AutomobileVO)
admin.site.register(Customer)
admin.site.register(SalesPerson)
admin.site.register(SalesRecord)
